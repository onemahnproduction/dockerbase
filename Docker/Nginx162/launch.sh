#!/bin/bash

phpPort="$1"
eval phpIp="\$FPM_PORT_${phpPort}_TCP_ADDR"

nginxBalancerIP="$2"

nginxMasterIP="$3"
nginxSlaveIP="$4"

FILE="/etc/nginx/conf.d/fpm"
FILE_DEFAULT="${FILE}.default"

NGINX_FILE="/etc/nginx/nginx.conf"
NGINX_FILE_DEFAULT="${NGINX_FILE}.default"

LOAD_BALANCE_FILE="/etc/nginx/conf.d/loadbalancer"
LOAD_BALANCE_FILE_DEFAULT="${LOAD_BALANCE_FILE}.default"

if [ -f $FILE_DEFAULT ]; then
    cp $FILE_DEFAULT $FILE
    sed -i "s/%fpm-ip%/${phpIp}:${phpPort}/" $FILE
    sed -i "s/%slave-ip%/${nginxSlaveIP}/" $FILE
fi

if [ -f $LOAD_BALANCE_FILE_DEFAULT ]; then
    cp $LOAD_BALANCE_FILE_DEFAULT $LOAD_BALANCE_FILE
fi

if [ -f $NGINX_FILE_DEFAULT ]; then
    cp $NGINX_FILE_DEFAULT $NGINX_FILE
    sed -i "s/%nginx-balancer-ip%/${nginxBalancerIP}/" $NGINX_FILE
    sed -i "s/%nginx-master-ip%/${nginxMasterIP}/" $NGINX_FILE
    sed -i "s/%nginx-slave-ip%/${nginxSlaveIP}/" $NGINX_FILE
fi

/usr/sbin/nginx