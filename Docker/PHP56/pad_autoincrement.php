<?php
    
    $mariaDBIPAddress = get_cfg_var("mariaDBIPAddress");
    
    echo $mariaDBIPAddress;
    
    
    $con=mysqli_connect($mariaDBIPAddress, "ematic_plat_dev", 'em@tics$olutions', "EmaticPlatformDev");
    /* Set MySQL communication encoding */
    mysqli_set_charset($con, "UTF8");
    mysqli_query($con, "SET NAMES utf8");
    mysqli_query($con, "SET CHARACTER SET utf8");
    
    
    // Check connection
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MariaDB: " . mysqli_connect_error() . "\n";
        
        exit();
    } else {
        echo "Successfully connected to MariaDB\n";
    }
    
    
    function db_couldnotrun($con, $debug) {
        if($debug) {
            echo 'Could not run query: ' . mysqli_error($con) . "\n";
        }
        exit;
    }
    
    function db_query($con, $query, $debug) {
        if($debug) { echo "$query\n"; }
        if ($result = mysqli_query($con, $query)) {
            return $result;
        } else {
            db_couldnotrun($con, $debug);
        }
    }
    
    //MYSQLI_ASSOC, MYSQLI_NUM
    function db_query_for_rows ($con, $query, $resultType, $debug) {
        $result = db_query($con, $query, $debug);
        $rows = array();
        while($r = mysqli_fetch_array($result, $resultType)) {
            $rows[] = $r;
        }
        
        db_result_free($con, $result, $debug);
        
        return $rows;
    }
    
    function db_query_for_json_encode ($con, $query, $debug) {
        $result = db_query($con, $query, $debug);
        $rows = array();
        
        while($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        
        db_result_free($con, $result, $debug);
        
        return $rows;
    }
    
    function db_result_free($con, $result, $debug) {
        mysqli_free_result($result);
        
        while(mysqli_more_results($con) &&  mysqli_next_result($con)) {
            if($result = mysqli_use_result($con)) {
                mysqli_free_result($result);
            }
            
        }
    }
    
    function db_query_exists($con, $query, $debug) {
        if($debug) { echo "$query\n"; }
        if ($result = mysqli_query($con, $query)) {
            $rowLength = mysqli_num_rows($result);
            
            db_result_free($con, $result, $debug);
            
            return $rowLength;
        } else {
            db_couldnotrun($con, $debug);
        }
    }
    
    function db_query_for_value($con, $query, $debug) {
        if($debug) { echo "$query\n"; }
        if ($result = mysqli_query($con, $query)) {
            
            $rows = mysqli_num_rows($result);
            $value = "";
            
            if($rows) {
                $row = mysqli_fetch_array($result, MYSQLI_NUM);
                $value = $row[0];
            }
            
            db_result_free($con, $result, $debug);
            
            return $value;
        } else {
            db_couldnotrun($con, $debug);
        }
    }
    
    function getNewAutoIncrementAlterStatement($con, $table) {
        $query = "SELECT id FROM $table ORDER BY id DESC LIMIT 1";
        $autoIncrementValue = db_query_for_value($con, $query, 1);
        $newAutoIncrementValue = $autoIncrementValue + 1000000;
        echo "$autoIncrementValue : $newAutoIncrementValue\n";
        
        $query = "ALTER TABLE $table AUTO_INCREMENT = $newAutoIncrementValue";
        
        if(!db_query($con, $query, 1)) {
            echo "failed\n";
            exit(0);
        }
        
        return $query."\n";
    }
    
    $queryAll = "";
    $queryAll .= getNewAutoIncrementAlterStatement($con, "TreatmentSubjects");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "ControlActivities");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "ControlSubjects");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "Cookies");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "DeviceScreenSizes");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "Browsers");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "OperatingSystems");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "Visits");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "Details");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "Activities");
    $queryAll .= getNewAutoIncrementAlterStatement($con, "Products");
    
    echo $queryAll;
    
?>