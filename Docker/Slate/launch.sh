#!/bin/bash

#serverIpAddres=`ip addr | grep eth0 | grep inet | awk -F" " '{print $2}'| sed -e 's/\/.*$//'`

#set -eo pipefail
#echo "Starting consul agent"
#
#consul agent -config-dir=/config -join=$DOCKERBASE_CONSUL_SERVER_IP &
#
#curl -X PUT -d "$serverIpAddres" http://$DOCKERBASE_CONSUL_SERVER_IP:8500/v1/kv/slate/$EMATIC_ENV/ip



if [ ! -f /var/slate/slate/Gemfile ]; then
cd /var/slate && git clone https://github.com/tripit/slate.git slate
cd /var/slate/slate && bundle install
fi

#https://github.com/middleman/middleman/issues/730
cd /var/slate/slate && bundle exec middleman server --force-polling