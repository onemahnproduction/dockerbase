#!/bin/bash

rm -fr /var/slate_temp
cp -R /var/slate/slate /var/slate_temp
cd /var/slate_temp && git init
cd /var/slate_temp && rake build

rm -fr /var/slate/slate/build
cp -R /var/slate_temp/build /var/slate/slate/
cd /var/slate/slate/ && gzip -3 -c -r build > build.`date +%Y-%m-%d_%H-%M-%S`.gz
rm -fr /var/slate_temp

