#!/bin/bash

environment=$1
method=$2
image=$3

projectPath=$(cd .. && pwd)
projectDockerPath=$(pwd)
wwwPath=$(find "$projectPath" -name www 2>/dev/null)
dataContainerDockerPath=$(find "$projectDockerPath" -name DataContainer 2>/dev/null)
jsonResumeDockerPath=$(find "$projectDockerPath" -name JSONResume 2>/dev/null)
baseDockerPath=$(find "$projectDockerPath" -name Base 2>/dev/null)
consulDockerPath=$(find "$projectDockerPath" -name Consul 2>/dev/null)
gwanDockerPath=$(find "$projectDockerPath" -name Gwan 2>/dev/null)
mariaDBDockerPath=$(find "$projectDockerPath" -name MariaDB10 2>/dev/null)
php56DockerPath=$(find "$projectDockerPath" -name PHP56 2>/dev/null)
perl5DockerPath=$(find "$projectDockerPath" -name PERL516 2>/dev/null)
nginxDockerPath=$(find "$projectDockerPath" -name Nginx162 2>/dev/null)
nodeJSDockerPath=$(find "$projectDockerPath" -name "NodeJSv5.10.1" 2>/dev/null)
slateDockerPath=$(find "$projectDockerPath" -name Slate 2>/dev/null)

prefix="base_"
suffix_storage="persistent_data"



portForwardVirtualBox() {
    echo "boot2docker VirtualBox port forwarding hack for OS X to enable localhost access"

#    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port80,tcp,,80,,80";
#    VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port80,udp,,80,,80";

    for i in {49000..49900}; do
        VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port$i,tcp,,$i,,$i";
        VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port$i,udp,,$i,,$i";
    done

    #port forwarding for consul in dev
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port8300,tcp,,8300,,8300";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port8301,tcp,,8301,,8301";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port8301,udp,,8301,,8301";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port8302,tcp,,8302,,8302";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port8302,udp,,8302,,8302";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port8400,tcp,,8400,,8400";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port8500,tcp,,8500,,8500";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port53,udp,,53,,53";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port4444,tcp,,4444,,4444";
}

portReverseVirtualBox() {
    echo "boot2docker VirtualBox port forwarding hack for OS X to enable localhost access"

#    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port80";
#    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "udp-port80";

    for i in {49000..49900}; do
        VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port$i";
        VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "udp-port$i";
    done

    #port forwarding for consul in dev
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port8300";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port8301";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "udp-port8301";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port8302";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "udp-port8302";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port8400";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port8500";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "udp-port53";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port4444";
}


updocker() {
    if [ "$(uname)" == "Darwin" ]; then
    # Do something under Mac OS X platform

        # make sure docker is restarted
        echo "make sure docker is down..."
        boot2docker down
        boot2docker poweroff

        echo "boot2docker up"
        boot2docker up > boot2docker_up.out

        echo "export environment variables..."
        cat boot2docker_up.out
        cat boot2docker_up.out | grep export | sed 's/^ *//; s/; */;/g' > exports
        rm boot2docker_up.out
        source exports
        echo "# docker $(date)" >> $HOME/.profile
        cat exports >> $HOME/.profile


    #elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
        # Do something under Linux platform
    #elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
        # Do something under Windows NT platform
    fi

    echo "boot: succes"
}

downdocker() {
    echo "boot2docker down..."
    boot2docker down

    echo "boot2docker poweroff..."
    boot2docker poweroff

    echo "shutdown: succes"
}

install() {

#    if [ "$(uname)" == "Darwin" ]; then
#        # Do something under Mac OS X platform
#
#        echo "boot2docker down"
#        boot2docker down
#
#        echo "boot2docker destroy"
#        boot2docker destroy
#
#        echo "boot2docker init"
#        boot2docker init
#
#        portForwardVirtualBox
#
#        echo "boot2docker up"
#        boot2docker up > boot2docker_up.out
#
#        # take care of exports
#        echo "take care of env exports"
#
#        cat boot2docker_up.out
#        cat boot2docker_up.out | grep export | sed 's/^ *//; s/; */;/g' > exports
#        rm boot2docker_up.out
#        source exports
#        echo "# docker $(date)" >> $HOME/.profile
#        cat exports >> $HOME/.profile
#
#    #elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
#        # Do something under Linux platform
#    #elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
#        # Do something under Windows NT platform
#    fi

    compileImage "storage" "busybox:storage" "$dataContainerDockerPath"
    compileImage "EmaticBase" "centos:EmaticBase" "$baseDockerPath"

    case $image in
        node)
            compileImage "5.10.1" "nodejs:5.10.1" "$nodeJSDockerPath"
            ;;
        php)
            #compileImage "json_resume" "centos:json_resume" "$jsonResumeDockerPath"
            compileImage "PHP56" "centos:PHP56" "$php56DockerPath"
            compileImage "Nginx162" "centos:Nginx162" "$nginxDockerPath" #docker build -rm=true -t centos:Nginx162 .
            compileImage "MariaDB10" "centos:MariaDB10" "$mariaDBDockerPath"
            #compileImage "NodeJS012" "centos:NodeJS012" "$nodeJSDockerPath"
            #compileImage "PERL516" "centos:PERL516" "$perl5DockerPath"
            #compileImage "Slate" "centos:Slate" "$slateDockerPath"
            ;;
        *)
            exit 1
    esac


#    if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
#        cp mariaDB.repo /etc/yum.repos.d/MariaDB.repo
#        yum install -y MariaDB-client
#    fi

    echo "install: success"

}

unload() {
    docker rm -f $prefix$suffix_consul
    docker rm -f $prefix$suffix_storage
}

load() {
    loadStorage

}



loadStorage() {
    #start storage container
    echo "start storage container..."
    docker run --name $prefix$suffix_storage -h $prefix$suffix_storage busybox:storage true
}



compileImage() {

    ID=$(docker images | grep $1 | awk '{print $3}')

    if [[ -z "$ID" ]]; then
        echo "creating $2"
        (cd "$3"; docker build --rm=true -t "$2" .)
    fi
}




### main logic ###
case "$method" in
    install)
        install
        ;;
    updocker)
        updocker
        ;;
    load)
        load
        ;;
    unload)
        unload
        ;;
    downdocker)
        stop
        unload
        downdocker
        ;;
    portForwardVirtualBox)
        portForwardVirtualBox
        ;;
    portReverseVirtualBox)
        portReverseVirtualBox
        ;;
    *)
        echo $"Usage: $0 {prod|staging|dev} {bootdocker|shutdowndocker|install|load|unload|reload|start|stop|restart|status}"
        exit 1
esac
exit 0










