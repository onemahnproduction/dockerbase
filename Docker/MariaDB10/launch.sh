#!/bin/bash



#push mysql server ip address to PHP server
serverIpAddres=`ip addr | grep eth0 | grep inet | awk -F" " '{print $2}'| sed -e 's/\/.*$//'`

FILE=/etc/remotePHP/php.ini.default

if [ -f $FILE ]; then
    \cp /etc/remotePHP/php.ini.default /etc/remotePHP/php.ini
    echo "mariaDBIPAddress=${serverIpAddres}" >> /etc/remotePHP/php.ini
fi


#set -eo pipefail
#echo "Starting consul agent"

#consul agent -config-dir=/config -join=$DOCKERBASE_CONSUL_SERVER_IP &
#
#curl -X PUT -d "$serverIpAddres" http://$DOCKERBASE_CONSUL_SERVER_IP:8500/v1/kv/mariadb10/$DOCKERBASE_ENV/ip


# if the /data/mariadb directory doesn't contain anything, then initialise it
directory="/var/lib/mysql"
if [ ! "$(find $directory \( ! -regex '.*/\..*' \) -type f)" ]; then
/usr/bin/mysql_install_db --user=mysql
/usr/bin/mysqld_safe --init-file=/etc/my.cnf.d/mariadb-setup.sql
else
/usr/bin/mysqld_safe
fi



